//
//  RssItem.h
//  RssReader
//
//  Created by steve zhou on 3/9/15.
//  Copyright (c) 2015 Steve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RssItem : NSObject

@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *link;
@property(nonatomic, strong) NSString *contentDescription;
@property(nonatomic, strong) NSString *pubDate;

@end

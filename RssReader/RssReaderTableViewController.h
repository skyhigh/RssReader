//
//  RssReaderTableViewController.h
//  RssReader
//
//  Created by steve zhou on 3/9/15.
//  Copyright (c) 2015 Steve. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RssReaderTableViewController : UITableViewController<NSXMLParserDelegate>

@end
